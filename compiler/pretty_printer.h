#ifndef _PRETTY_PRINTER_H
#define _PRETTY_PRINTER_H

#include <stdio.h>
#include <stdlib.h>
#include "grammar.h"
#include "lib/list.h"

extern void print (struct class_t* prog_);
void print_classVarDec (struct classVarDec_t* var_);


void print_subroutineDec (struct subroutineDec_t* routine_);

void print_subroutineBody (struct subroutineBody_t* body);
void print_statements (struct statement_t* st_);


#endif
