#include "pretty_printer.h"

void print (struct class_t* prog_) {
  printf ("class name: %s\n", prog_->className);
  
  int length_var = list_length (prog_->classVarDec);
  int i;
  for (i = 0; i< length_var; i++) {
    struct classVarDec_t* elem = list_element (i, prog_->classVarDec);
    print_classVarDec (elem);
  }
  int length_routine = list_length (prog_->subroutineDec);
  for (i = 0; i< length_routine; i++) {
    struct subroutineDec_t* elem = list_element (i, prog_->subroutineDec);
    print_subroutineDec (elem);
  }
}

void print_classVarDec (struct classVarDec_t* var_) {
  switch (var_->keyword) {
    case _STATIC:
      printf ("\tstatic\n");
    break;
    case _FIELD:
      printf ("\tfield\n");
    break;
  }
  printf ("\t\t%s\n", var_->type);
  int length_var = list_length (var_->varName);
  int i;
  for (i = 0; i<length_var; i++) { 
    char* str = list_element (i, var_->varName);
    printf ("\t\t\t%s\n", str);
  }
  
}

void print_subroutineDec (struct subroutineDec_t* dec_) {
  switch (dec_->funKeyword) {
    case _CONSTRUCTOR:
      printf ("\tconstructor\n");
      break;
    case _FUNCTION:
      printf ("\tfunction\n");
      break;
    case _METHOD:
      printf ("\tmethod\n");
      break;
  }
  printf ("\t\t%s\n", dec_->type);
  printf ("\t\t%s\n", dec_->name);
  int i;
  int length_arg = list_length (dec_->arg);
  if (length_arg != 0)
    printf ("\t\targuments\n");
  for (i = 0; i< length_arg; i++) {
    struct parameter_t* elem = list_element (i, dec_->arg);
    printf ("\t\t\t%s\n", elem->name); 
    printf ("\t\t\t\t%s\n", elem->type); 
  }
  
  print_subroutineBody (dec_->body);
}

void print_subroutineBody (struct subroutineBody_t* body) {
  int i;
  int length_var = list_length (body->vars);
  
  for (i = 0; i< length_var; i++) {
    struct var_t* elt = list_element (i, body->vars);
    printf ("\t\t\t%s\n", elt->type);
    int j;
    int length_name = list_length (elt->name);
    for (j = 0; j< length_name; j++){
      char* elt_name = list_element (j, elt->name);
      printf ("\t\t\t%s\n", elt_name);
    }
  }

  int length_statements = list_length (body->statements);
  for (i = 0; i < length_statements; i++) {
    struct statement_t* st = list_element (i, body->statements);
    print_statements (st);
  }
    
}

void print_vars (struct var_t* var_) {

}

void print_statements (struct statement_t* st_) {
  switch (st_->type) {
    case _LET:
      printf ("let statement\n");
    break;
    case _IF:
      printf ("if statement\n");
    break;
    case _WHILE:
      printf ("while statement\n");
    break;
    case _DO:
      printf ("do statement: %s\n",st_->st.do_.call->method);
    break;
    case _RETURN:
      printf ("return statement\n");
    break;
  }
  
}
