#ifndef _LIST_H
#define _LIST_H

#include <stdio.h>
#include <stdlib.h>


/* element structure pointer */
struct s_list_elt {
  void *data;
  struct s_list_elt* p;
};

typedef struct s_list_elt t_list;
typedef struct s_list_elt* p_list;

/* list structure pointer */
struct s_list {
  p_list first;
  int length;
};

typedef struct s_list list;
typedef struct s_list* List;

/* creator */
List list_create ();

/* basic operations */
int list_insert (const int pos, void* elt,  List l);
void* list_element (const int pos, List l);
int list_remove (const int pos,  List l);
int list_destroy (List l);

/* length */
int list_length (List l);


#endif /* LIST_H */
