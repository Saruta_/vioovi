#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#include <stdio.h>
#include <stdlib.h>

/* basic hash function */

static char brotate (char byte, int nmb);
static char bcompression (char* key);
static char hash_function (char*key);

/* internal data structure */



/* list which manages collisions */

#define INT "int"
#define CHAR "char"
#define BOOLEAN "boolean"

enum kind_e {
  STATIC,
  FIELD,
  ARGUMENT,
  LOCAL
};

struct hash_elt_t{
  char* key;
  char* type;
  int value;
  int count;
};

#endif
