#include "hashtable.h"

static char brotate (char byte, int nmb) {
  char tmp = 0;

  int i;
  for (i = 0; i < (nmb%8); i++) {
    tmp = (0x80 & byte) >> 7; 
    byte <<= 1;
    byte += (tmp & 0x01); 
  }

  return byte;
}

static char bcompression (char* key) {
  
  char output = key[0]; 
    
  int i;
  for (i = 1; i!= '\0'; i++) {
    output ^= brotate (key[i],i); 
  }
  return output;
}
