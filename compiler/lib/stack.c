#include "stack.h"

Stack stack_create () {
  Stack stack = malloc (sizeof(struct t_stack));
  stack->first = NULL;
  stack->length = 0;
  return stack;
}

void stack_push (void* elt, Stack s) {
  if (stack_is_empty (s)) {
    p_stack_elt e = malloc (sizeof (t_stack_elt));
    e->data = elt;
    e->p = NULL;
    s->first = e;
    s->length ++;
  } else {
    p_stack_elt new = malloc (sizeof (t_stack_elt));
    new->data = elt;
    new->p = s->first;
    s->first = new;
    s->length++;
  }
}

void* stack_top (Stack s) {
  if (stack_is_empty (s)) 
    return NULL;
  else  
    return s->first->data;
}

void* stack_pop (Stack s) {
  if (stack_is_empty (s)) {
    return NULL;
  } else {
    p_stack_elt old = s->first;
    s->first = old->p;
    void* elt = old->data;
    free (old);
    s->length--;
    return elt;
  }
}

void stack_destroy (Stack s) {
  if (stack_is_empty (s)) {
    free (s);
  } else {
    while (!stack_is_empty (s)) {
      stack_pop (s);  
    }
  }
}

int stack_is_empty (Stack s) {
  if (s->length == 0)
    return 1;
  else
    return 0;
}
