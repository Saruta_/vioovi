#include "list.h"

List list_create (){
  List s = malloc (sizeof(list));
  s->first = NULL;
  s->length = 0;
  return s;
}

int list_insert (const int pos, void* elt, List l) {
  if (l->length < pos) {
    return 1;
  } else {
    p_list new  = malloc (sizeof(t_list));
    new->data = elt;
    if (pos == 0) {
      new->p = l->first;
      l->first = new;
      l->length++;
    } else {
      int i = 0;
      p_list s = l->first;
      while (s->p != NULL && i < (pos-1)) {
        s = s->p;
        ++i;
      }
      new->p = s->p;
      s->p = new;
      l->length++;
    }
  }

  return 0;
}

void* list_element (const int pos, List l) {
  if (l->length <= pos){
    return NULL;
  } else {
    p_list s = l->first;
    int i = 0;
    while (s->p != NULL && i < pos) {
      s = s->p;
      ++i;
    }
    if (i == pos)
      return s->data;
    else
      return NULL;
  }

  return 0;
}

int list_remove (const int pos, List l) {
  if (l->length <= pos) {
    return 1;
  } else {
    if (pos == 0) {
      p_list s = l->first;
      l->first = l->first->p;
      free (s);

      l->length--;

    } else {
      int i = 0;
      p_list s = l->first;
      p_list to_del;
      while (s->p != NULL && i < (pos-1)) {
        s = s->p;
        ++i;
      }
      to_del = s->p;
      s->p = to_del->p;
    
      l->length--;

      free (to_del);
    }
  }

  return 0;
}

int list_destroy (List l) {
  while (l->first != NULL) {
    list_remove (0, l);
  }
  
  free (l);
  return 0;
}

int list_length (List l) {
  return l->length;
}

