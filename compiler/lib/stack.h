#ifndef _STACK_H
#define _STACK_H

#include <stdio.h>
#include <stdlib.h>

/* element structure pointer */
struct s_stack_elt {
  void *data;
  struct s_stack_elt* p;
};

typedef struct s_stack_elt t_stack_elt;
typedef struct s_stack_elt* p_stack_elt;

struct t_stack {
  p_stack_elt first;
  int length;
};

typedef struct t_stack* Stack;

/* creator */
Stack stack_create ();

/* basic operations */
void stack_push (void* elt, Stack s);
void* stack_top (Stack s);
void* stack_pop (Stack s);
void stack_destroy (Stack s);

/* length operations */
int stack_is_empty (Stack s);

#endif /* _STACK_H */
