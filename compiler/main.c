#include <stdio.h>
#include <stdlib.h>
#include "parser.tab.h"
#include "pretty_printer.h"
#include "lib/list.h"

extern FILE* yyin;
struct class_t* prog_root; 
int yyparse();

int main(int argc, char* argv[]){
  --argc, ++ argv;
  if (!argc){
    printf ("error");
    return EXIT_FAILURE;
  }

  FILE* f = fopen(argv[0], "r");
  if (f == NULL){
    printf("error");
    return EXIT_FAILURE;
  }
  yyin = f;


  yyparse();

  print (prog_root);

  return EXIT_SUCCESS;
}
