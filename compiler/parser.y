%{
  #include <math.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include "lib/list.h"
  #include "grammar.h"
  int yyflex();
  void yyerror(char const*);
  char* global_value;
  extern struct class_t* prog_root;

%}

%union{
  int i;
  char* str;
  struct s_list* lst;
  struct classVarDec_t* var_dec;
  struct subroutineDec_t* subroutineDec;
  struct parameter_t* parameter;
  struct subroutineBody_t* subroutineBody;
  struct statement_t* statement;
  struct let_t* let_;
  struct if_t* if_;
  struct while_t* while_;
  struct do_t* do_;
  struct return_t* return_;
  struct expression_t* expression;
  struct term_t* term;
  struct binop_t* binop;
  struct subroutineCall_t* subroutineCall;
  struct prog_t* prog;
  struct var_t* var;
}

%type <prog> prog
%type <str> className 
%type <str> varName
%type <lst> classVarDecList
%type <lst> subroutineDecList
%type <lst> varNameList
%type <var_dec> classVarDec
%type <str> type;
%type <i> funKeyword
%type <str> typeOrVoid
%type <subroutineDec> subroutineDec
%type <str> subroutineName
%type <lst> parameterList
%type <parameter> parameter;
%type <subroutineBody> subroutineBody
%type <lst> varDec
%type <lst> statements
%type <statement> statement
%type <let_> letStatement
%type <if_> ifStatement
%type <while_> whileStatement
%type <do_> doStatement
%type <return_> returnStatement
%type <expression> expression
%type <i> keywordConstant
%type <i> unaryOp
%type <term> term
%type <binop> binop
%type <subroutineCall> subroutineCall
%type <lst> expressionList
%type <lst> expressionListBis
%type <var> varDecSolo

%start prog

%token CLASS CONSTRUCTOR FUNCTION METHOD 
%token FIELD VAR INT CHAR BOOLEAN VOID STATIC
%token TRUE FALSE NULL_ THIS LET DO IF ELSE WHILE RETURN
%token L_BRACKET R_BRACKET L_PARENTHESIS R_PARENTHESIS L_BRACE R_BRACE
%token PERIOD COMMA SEMICOLON 
%token AMPERSAND PIPE LT GT EQUAL TILDE 
%token <str> ID
%token <str> STRING
%token <str> INTEGER

%left PLUS MINUS TIMES DIVIDED 


%%
prog:   CLASS className L_BRACE classVarDecList subroutineDecList
    R_BRACE{prog_root = make_class($2, $4, $5);}
  ;

classVarDec:  STATIC type varNameList SEMICOLON 
           {$$ = attribut_dec (_STATIC, $2, $3);}
            | FIELD type varNameList SEMICOLON 
            {$$ = attribut_dec (_FIELD, $2, $3);}
            ;

classVarDecList: /* nothing */ {$$ = empty_attribut_list();}
              | classVarDecList classVarDec {$$ = $1; push_attribut ($$, $2);}
              ;

subroutineDecList: /* nothing */ {$$ = empty_routine_list();}
            | subroutineDecList subroutineDec{$$ = $1; push_routine ($$, $2);}
            ;

subroutineDec: funKeyword typeOrVoid subroutineName 
  L_PARENTHESIS R_PARENTHESIS L_BRACE subroutineBody R_BRACE
  {List lst = list_create(); $$ = make_subroutine($1, $2, $3, lst, $7);}
            |  funKeyword typeOrVoid subroutineName
  L_PARENTHESIS parameterList R_PARENTHESIS L_BRACE subroutineBody R_BRACE
  {$$ = make_subroutine($1, $2, $3, $5, $8);}
            ;

funKeyword: CONSTRUCTOR {$$ = _CONSTRUCTOR;}
          | FUNCTION {$$ = _FUNCTION;}
          | METHOD {$$ = _METHOD;}
          ;

typeOrVoid: VOID {$$ = _VOID;}
          | type {$$ = $1;}
          ;

className: ID{$$ = $1;}
        ;

parameterList: parameterList COMMA parameter {$$ = $1; push_parameter ($$, $3);}
            | parameter {$$ = dec_parameter_list ($1);}
          ;

subroutineBody: varDec statements {$$ = make_routine_body ($1, $2);}
          ;

parameter: type varName {$$ = make_parameter($1, $2);}
        ;

varDec: /* nothing */ {$$ = empty_var_list();}
      | varDec varDecSolo {$$ = $1; push_var ($$, $2);}
      ;

varDecSolo: VAR type varNameList SEMICOLON {$$ = make_var_dec_solo($2, $3);}

varNameList: varName {$$ = dec_attribut_name_list ($1);}
          |  varNameList COMMA varName {$$ = $1; push_name_attribut ($$, $3);}
          ;

varName: ID{$$ = $1;}
      ;

subroutineName: ID{$$ = $1;}
      ;

type: INT {$$ = _INT;}
    | CHAR {$$ = _CHAR;}
    | BOOLEAN {$$ = _BOOLEAN;}
    | className {$$ = $1;}
    ;

/* statements */

statements: /* nothing */ {$$ = empty_statement_list ();}
        | statements statement {$$ = $1; push_statement ($$, $2);}
        ;

statement: letStatement {$$ = make_let_statement($1);}
          | ifStatement {$$ = make_if_statement($1);}
          | whileStatement {$$ = make_while_statement($1);}
          | doStatement {$$ = make_do_statement($1);}
          | returnStatement {$$ = make_return_statement($1);}
          ;

letStatement: LET varName EQUAL expression SEMICOLON 
            {$$ = make_let_block($2, NULL, $4);}
            | LET varName L_BRACKET expression R_BRACKET
          EQUAL expression SEMICOLON 
            {$$ = make_let_block($2, $4, $7);}
          ;

ifStatement: IF L_PARENTHESIS expression R_PARENTHESIS 
    L_BRACE statements R_BRACE 
            {$$ = make_if_block($3, $6, NULL);}
          |  IF L_PARENTHESIS expression R_PARENTHESIS
    L_BRACE statements R_BRACE ELSE L_BRACE statements R_BRACE 
            {$$ = make_if_block($3, $6, $10);}
          ;

whileStatement: WHILE L_PARENTHESIS expression R_PARENTHESIS 
    L_BRACE statements R_BRACE {$$ = make_while_block($3, $6);}
              ;

doStatement: DO subroutineCall SEMICOLON {$$ = make_do_block($2);}
            ;

returnStatement: RETURN SEMICOLON {$$ = make_return_block(NULL);}
              | RETURN expression SEMICOLON {$$ = make_return_block($2);}
              ;

/* expressions */

expression:   term {$$ = make_term_expression($1);}
            | binop {$$ = make_binop_expression($1);}
            ;

binop:      expression PLUS expression {$$ = make_binop_block($1, _PLUS, $3);}
          | expression MINUS expression {$$ = make_binop_block($1, _MINUS, $3);}
          | expression TIMES expression {$$ = make_binop_block($1, _TIMES, $3);}
          | expression DIVIDED expression 
          {$$ = make_binop_block($1, _DIVIDED, $3);}
          | expression AMPERSAND expression 
          {$$ = make_binop_block($1, _AMPERSAND, $3);}
          | expression PIPE expression {$$ = make_binop_block($1, _PIPE, $3);}
          | expression LT expression {$$ = make_binop_block($1, _LT, $3);}
          | expression GT expression {$$ = make_binop_block($1, _GT, $3);}
          | expression EQUAL expression {$$ = make_binop_block($1, _EQUAL, $3);}
          ;

term: INTEGER {$$ = make_int_term($1);} 
    | STRING {$$ = make_string_term($1);} 
    | keywordConstant {$$ = make_keyword_term($1);}
    | varName {$$ = make_var_term($1);} 
    | varName L_BRACKET expression R_BRACKET {$$ = make_array_term($1, $3);}
    | subroutineCall {$$ = make_call_term($1);} 
    | L_PARENTHESIS expression R_PARENTHESIS {$$ = make_parenth_term($2);}
    | unaryOp term {make_unary_term($1, $2);}
    ;

subroutineCall: subroutineName L_PARENTHESIS expressionList R_PARENTHESIS 
              {$$ = make_call(NULL, $1, $3);}
              | className PERIOD subroutineName 
            L_PARENTHESIS expressionList R_PARENTHESIS 
            {$$ = make_call($1, $3, $5);}
              | varName PERIOD subroutineName
            L_PARENTHESIS expressionList R_PARENTHESIS 
            {$$ = make_call($1, $3, $5);}
            ;

expressionList: /* nothing */ {$$ = NULL;}
              | expressionListBis {$$ = $1;}
              ;

expressionListBis: expression {$$ = single_expression_list($1);}
              | expressionListBis COMMA expression 
              {$$ = $1; push_expression($$, $3);}
              ;

unaryOp: MINUS {$$ = _INV;}
      | TILDE {$$ = _TILDE;}
      ;

keywordConstant: TRUE {$$ = _TRUE;}
              |  FALSE {$$ = _FALSE;}
              |  NULL_ {$$ = _NULL;}
              |  THIS {$$ = _THIS;}
              ;

%%

void yyerror(char const* str){
  printf ("ca marcheu pa");
}
/*
int main(){
  yyparse();
  return 0;
}
*/
