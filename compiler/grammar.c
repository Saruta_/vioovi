#include "grammar.h"

struct class_t *make_class (char* className_p, List classVarDec_p, 
    List subroutineDec_p) {
  struct class_t *class = malloc (sizeof(struct class_t));
  class->className = className_p;
  class->classVarDec = classVarDec_p;
  class->subroutineDec = subroutineDec_p;
  return class;
}

List empty_attribut_list () {
  return list_create ();
}

void push_attribut (List lst, struct classVarDec_t* var) {
  int len = list_length (lst);
  list_insert (len, var, lst); 
}

struct classVarDec_t *attribut_dec (enum varKeyword_e keyword_p, char* type_p, 
    List varName_p) {
  struct classVarDec_t* varDec = malloc (sizeof (struct classVarDec_t));
  varDec->keyword = keyword_p;
  varDec->type = type_p;
  varDec->varName = varName_p;
  return varDec;
}


List dec_attribut_name_list (char* first) {
  List lst = list_create ();
  list_insert (0, first, lst);
  return lst;
}

void push_name_attribut (List lst, char* name) {
  int len = list_length (lst);
  list_insert (len, name, lst);
}

List empty_routine_list () {
  return list_create ();
}

void push_routine (List lst, struct subroutineDec_t* subroutineDec_p) {
  int len = list_length (lst);
  list_insert (len, subroutineDec_p, lst);
}

struct subroutineDec_t* make_subroutine (int fun_p, 
    char* type_p, char* name_p, List arg_p, 
    struct subroutineBody_t* body_p) {
  struct subroutineDec_t* routine = malloc (sizeof (struct subroutineDec_t));
  routine->funKeyword = fun_p;
  routine->type = type_p;
  routine->name = name_p;
  routine->arg = arg_p;
  routine->body = body_p;
  return routine;
}

List dec_parameter_list (struct parameter_t *parameter_p) {
  List list = list_create ();
  list_insert (0, parameter_p, list);
  return list;
}

void push_parameter (List lst, struct parameter_t* parameter_p){
  int len = list_length (lst);
  list_insert (len, parameter_p, lst);
}

struct parameter_t* make_parameter (char* type_p, char* name_p) {
  struct parameter_t* parameter = malloc (sizeof (struct parameter_t));
  parameter->type = type_p;
  parameter->name = name_p;
  return parameter;
}

struct subroutineBody_t* make_routine_body (List vars_p, List st_p) {
  struct subroutineBody_t* routine_body = 
    malloc (sizeof (struct subroutineBody_t));
  routine_body->vars = vars_p;
  routine_body->statements = st_p;
  return routine_body;
}

List empty_var_list () {
  return list_create ();
}

void push_var (List list, struct var_t* var_) {
  int len  = list_length (list);
  list_insert (len, var_, list);
}

struct var_t* make_var_dec_solo (char* type_, List args_) {
  struct var_t* var = malloc (sizeof (struct var_t));
  var->type = type_;
  var->name = args_;
  return var;
}

List empty_statement_list () {
  return list_create ();
}

void push_statement (List lst, struct statement_t* statement_p) {
  int len = list_length (lst);
  list_insert (len, statement_p, lst);
}

struct statement_t* make_if_statement (struct if_t* if_block){
  struct statement_t* statement = malloc(sizeof(struct statement_t));
  statement->type = _IF;
  statement->st.if_.condit_exp = if_block->condit_exp;
  statement->st.if_.if_statements = if_block->if_statements;
  statement->st.if_.else_statements = if_block->else_statements;
  free (if_block);
  return statement;
}

struct statement_t* make_let_statement (struct let_t* let_block){
  struct statement_t* statement = malloc(sizeof(struct statement_t));
  statement->type = _LET;
  statement->st.let_.name = let_block->name;
  statement->st.let_.array_exp = let_block->array_exp;
  statement->st.let_.exp = let_block->exp;
  free (let_block);
  return statement;
}

struct statement_t* make_while_statement (struct while_t* while_block){
  struct statement_t* statement = malloc(sizeof(struct statement_t));
  statement->type = _WHILE;
  statement->st.while_.while_exp = while_block->while_exp;
  statement->st.while_.statements = while_block->statements;
  free (while_block);
  return statement;
}

struct statement_t* make_do_statement (struct do_t* do_block){
  struct statement_t* statement = malloc(sizeof(struct statement_t));
  statement->type = _DO;
  statement->st.do_.call = do_block->call;
  free (do_block);
  return statement;
}

struct statement_t* make_return_statement (struct return_t* return_block){
  struct statement_t* statement = malloc(sizeof(struct statement_t));
  statement->type = _RETURN;
  statement->st.return_.exp = return_block->exp;
  free (return_block);
  return statement;
}

struct if_t* make_if_block (struct expression_t* exp, List if_st, 
    List else_st) {
  struct if_t* if_block = malloc (sizeof(struct if_t));
  if_block->condit_exp = exp;
  if_block->if_statements = if_st;
  if_block->else_statements = else_st;
  return if_block;
}

struct let_t* make_let_block (char* name_p, struct expression_t* array_exp_p,
    struct expression_t* exp_p) {
  struct let_t* let_block = malloc (sizeof(struct let_t));
  let_block-> name = name_p;
  let_block->array_exp = array_exp_p;
  let_block->exp = exp_p;
  return let_block;
}

struct while_t* make_while_block (struct expression_t* exp_p, List st_p) {
  struct while_t* while_block = malloc (sizeof(struct while_t));
  while_block->while_exp = exp_p;
  while_block->statements = st_p;
  return while_block;
}

struct do_t* make_do_block (struct subroutineCall_t* call_p) {
  struct do_t* do_block = malloc (sizeof (struct do_t));
  do_block->call = call_p;
  return do_block;
}

struct return_t* make_return_block (struct expression_t* exp_p) {
  struct return_t* return_block = malloc (sizeof (struct return_t));
  return_block->exp = exp_p;
  return return_block;
}

struct expression_t* make_term_expression (struct term_t* term_p) {
  struct expression_t* exp = malloc (sizeof (struct expression_t));
  exp->kind = _TERM;
  exp->content.term = term_p;
  return exp;
}

struct expression_t* make_binop_expression (struct binop_t* binop_p) {
  struct expression_t* exp = malloc (sizeof (struct expression_t));
  exp-> kind = _BINOP;
  exp->content.binop = binop_p;
  return exp;
}

struct binop_t* make_binop_block (struct expression_t* left_p, enum op_e op_p,
    struct expression_t* right_p){
  struct binop_t* binop = malloc (sizeof (struct binop_t));
  binop->left = left_p;
  binop->op = op_p;
  binop->right = right_p;
  return binop;
}
 
struct term_t* make_int_term (char* integer_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _INTEGER;
  term->content.i = atoi(integer_p);
  return term;
}

struct term_t* make_string_term (char* str_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _STRING;
  term->content.str = str_p;
  return term;
}

struct term_t* make_keyword_term (int  keyword_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _KEYWORD;
  term->content.keyword = keyword_p;
  return term;
}

struct term_t* make_var_term (char* var_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _VARNAME;
  term->content.var = var_p;
  return term;
}

struct term_t* make_array_term (char* var_p, struct expression_t* exp_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _ARRAY;
  term->content.array.var = var_p;
  term->content.array.exp = exp_p;
  return term;
}

struct term_t* make_call_term (struct subroutineCall_t* call_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _CALL;
  term->content.call = call_p;
  return term;
}

struct term_t* make_parenth_term (struct expression_t* exp_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _PARENTHESIS;
  term->content.parenth = exp_p;
  return term;
}

struct term_t* make_unary_term (int unary_p,
    struct term_t* term_p) {
  struct term_t* term = malloc (sizeof (struct term_t));
  term->kind = _UNARY;
  term->content.unary.op = unary_p;
  term->content.unary.term = term_p;
  return term;
}

struct subroutineCall_t* make_call (char* class_p, char* method_p, List arg_p){
  struct subroutineCall_t* routine = malloc (sizeof(struct subroutineCall_t));
  routine->className = class_p;
  routine->method = method_p;
  routine->arguments = arg_p;
  return routine;
}

List single_expression_list (struct expression_t* exp_p){
  List lst = list_create ();
  list_insert (0, exp_p, lst);
  return lst;
}

void push_expression (List lst_p, struct expression_t* exp_p) {
  int length = list_length (lst_p);
  list_insert (length, exp_p, lst_p);
}
