%{
  #include <stdlib.h>
  #include <stdio.h>
  #include "grammar.h"
  #include "parser.tab.h"
  #include <string.h>

  #define YY_GLOBAL_VALUE (yylval.str = strdup(yytext))
%}

%option noyywrap
%x INLINE_COMMENTS

Id [a-zA-Z][a-zA-Z0-9]*
Integer [0-9]+
String \".*\"
%%
class {return CLASS;}
constructor {return CONSTRUCTOR;}
function {return FUNCTION;}
method {return METHOD;}
field {return FIELD;}
static {return STATIC;}
var {return VAR;}
int {return INT;}
char {return CHAR;}
boolean {return BOOLEAN;}
void {return VOID;}
true {return TRUE;}
false {return FALSE;}
null {return NULL_;}
this {return THIS;}
let {return LET;}
do {return DO;}
if {return IF;}
else {return ELSE;}
while {return WHILE;}
return {return RETURN;}
"[" {return L_BRACKET;}
"]" {return R_BRACKET;}
"(" {return L_PARENTHESIS;}
")" {return R_PARENTHESIS;}
"{" {return L_BRACE;}
"}" {return R_BRACE;}
"." {return PERIOD;}
"," {return COMMA;}
";" {return SEMICOLON;}
"+" {return PLUS;}
"-" {return MINUS;}
"*" {return TIMES;}
"/" {return DIVIDED;}
"&" {return AMPERSAND;}
"|" {return PIPE;}
"<" {return LT;}
">" {return GT;}
"=" {return EQUAL;}
"~" {return TILDE;}
{Id} {YY_GLOBAL_VALUE; return ID;}
{Integer} {YY_GLOBAL_VALUE; return INTEGER;}
{String} {YY_GLOBAL_VALUE; return STRING;}
[ \n\t] {} /* blanks */
<<EOF>> {return 0;}
.  {printf ("error!"); return -1;}
%%

/*int main(int argc, char* argv[]){
  --argc; ++argv;
  if (argc){
    FILE* f = fopen (argv[0], "r");
    if (f != NULL){
      yyin = f;
      int token = 0;
      do {
        token = yylex();
        printf ("code: %i; string: '%s'\n", token, yytext);
      } while (token != -1 && token != 0);
    } else {
      printf ("error input\n");
    }
  }
  else {
    printf ("error input\n");
  }
  return 0;
}*/
