/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    CLASS = 258,
    CONSTRUCTOR = 259,
    FUNCTION = 260,
    METHOD = 261,
    FIELD = 262,
    VAR = 263,
    INT = 264,
    CHAR = 265,
    BOOLEAN = 266,
    VOID = 267,
    STATIC = 268,
    TRUE = 269,
    FALSE = 270,
    NULL_ = 271,
    THIS = 272,
    LET = 273,
    DO = 274,
    IF = 275,
    ELSE = 276,
    WHILE = 277,
    RETURN = 278,
    L_BRACKET = 279,
    R_BRACKET = 280,
    L_PARENTHESIS = 281,
    R_PARENTHESIS = 282,
    L_BRACE = 283,
    R_BRACE = 284,
    PERIOD = 285,
    COMMA = 286,
    SEMICOLON = 287,
    AMPERSAND = 288,
    PIPE = 289,
    LT = 290,
    GT = 291,
    EQUAL = 292,
    TILDE = 293,
    ID = 294,
    STRING = 295,
    INTEGER = 296,
    PLUS = 297,
    MINUS = 298,
    TIMES = 299,
    DIVIDED = 300
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 14 "parser.y" /* yacc.c:1909  */

  int i;
  char* str;
  struct s_list* lst;
  struct classVarDec_t* var_dec;
  struct subroutineDec_t* subroutineDec;
  struct parameter_t* parameter;
  struct subroutineBody_t* subroutineBody;
  struct statement_t* statement;
  struct let_t* let_;
  struct if_t* if_;
  struct while_t* while_;
  struct do_t* do_;
  struct return_t* return_;
  struct expression_t* expression;
  struct term_t* term;
  struct binop_t* binop;
  struct subroutineCall_t* subroutineCall;
  struct prog_t* prog;
  struct var_t* var;

#line 122 "parser.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
