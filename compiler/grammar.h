#ifndef _GRAMMAR_H
#define _GRAMMAR_H

#include <stdio.h>
#include <stdlib.h>
#include "lib/list.h"

#define _INT "int"
#define _CHAR "char"
#define _BOOLEAN "boolean"
#define _VOID "void"

struct subroutineCall_t {
  char* className;
  char* method;
  List arguments;
};

#define _TRUE  0
#define _FALSE 1
#define _NULL 2
#define _THIS 3

#define _INV 4
#define _TILDE 5

struct term_t {
  enum {_INTEGER = 0, _STRING, _KEYWORD, _VARNAME, _ARRAY, _CALL, _PARENTHESIS,
  _UNARY} kind;
  union{
    int i;
    char* str;
    int keyword;
    char* var;
    struct {char* var; struct expression_t* exp;} array;
    struct subroutineCall_t* call;
    struct expression_t* parenth;
    struct {int op; struct term_t* term;} unary;
  } content;
};

struct expression_t {
  enum {_TERM, _BINOP} kind;
  union {
    struct binop_t *binop;
    struct term_t *term;
  } content;
};

enum op_e {_PLUS = 0, _MINUS, _TIMES, _DIVIDED, _AMPERSAND, _PIPE, _LT, _GT, 
  _EQUAL};

struct binop_t {
  struct expression_t* left;
  enum op_e op;
  struct expression_t* right;
};

struct return_t {
  /* if null 'return' else 'return exp;' */
  struct expression_t* exp;
};

struct do_t {
  struct subroutineCall_t* call;  
};

struct if_t {
  /* condition; */
  struct expression_t* condit_exp;
  /* statements list "if" */
  List if_statements;
  /* statements list "else" */
  List else_statements;
};

struct let_t {
  char* name;
  /* null if not array else array */
  struct expression_t* array_exp;
  struct expression_t* exp;
};

struct while_t {
  struct expression_t* while_exp;
  List statements;
};

struct statement_t {
  enum {_LET = 0, _IF, _WHILE, _DO, _RETURN} type;
  union {
    struct while_t while_;
    struct let_t let_;
    struct if_t if_;
    struct do_t do_;
    struct return_t return_;
  } st;
};

struct var_t {
  char* type;
  List name;
};

struct subroutineBody_t {
  List vars;
  List statements;
};

struct parameter_t {
  char* type;
  char* name;
};

#define _CONSTRUCTOR 6
#define _FUNCTION 7
#define _METHOD 8

struct subroutineDec_t {
  int funKeyword;
  char* type;
  char* name;
  List arg;
  struct subroutineBody_t *body;
};

enum varKeyword_e {
  _STATIC,
  _FIELD
};

struct classVarDec_t {
  enum varKeyword_e keyword;
  char* type;
  List varName;
};

struct class_t {
  char* className;
  List classVarDec;
  List subroutineDec;
};

struct class_t *make_class (char*, List, List);

List empty_attribut_list ();
void push_attribut (List, struct classVarDec_t*);
struct classVarDec_t *attribut_dec (enum varKeyword_e, char*, List);

List dec_attribut_name_list (char*);
void push_name_attribut (List, char* );

List empty_routine_list ();
void push_routine (List, struct subroutineDec_t*);

struct subroutineDec_t* make_subroutine (int, char*, char*, 
    List, struct subroutineBody_t*);

List dec_parameter_list (struct parameter_t*);
void push_parameter (List, struct parameter_t*);

struct parameter_t* make_parameter (char*, char*);

struct subroutineBody_t* make_routine_body (List, List);

List empty_var_list ();
void push_var (List, struct var_t*);

struct var_t* make_var_dec_solo (char*, List);

List empty_statement_list ();
void push_statement (List, struct statement_t*);

struct statement_t* make_if_statement(struct if_t*);
struct statement_t* make_let_statement(struct let_t*);
struct statement_t* make_while_statement(struct while_t*);
struct statement_t* make_do_statement(struct do_t*);
struct statement_t* make_return_statement(struct return_t*);

struct if_t* make_if_block(struct expression_t*,List,List);
struct let_t* make_let_block(char*, struct expression_t*, 
    struct expression_t*);
struct while_t* make_while_block(struct expression_t*,List);
struct do_t* make_do_block(struct subroutineCall_t*);
struct return_t* make_return_block(struct expression_t*);
 
struct expression_t* make_term_expression(struct term_t* term_p);
struct expression_t* make_binop_expression(struct binop_t* binop_p);

struct binop_t* make_binop_block(struct expression_t*, enum op_e,
    struct expression_t*);

struct term_t* make_int_term (char* integer_p);
struct term_t* make_string_term (char* str_p);
struct term_t* make_keyword_term (int);
struct term_t* make_var_term (char* var);
struct term_t* make_array_term (char* var, struct expression_t* exp_p);
struct term_t* make_call_term (struct subroutineCall_t* call_p);
struct term_t* make_parenth_term (struct expression_t* expression_p);
struct term_t* make_unary_term (int, struct term_t*);

struct subroutineCall_t* make_call (char* class_p, char* method_p, List arg_p); 

List single_expression_list(struct expression_t*);
void push_expression(List, struct expression_t*);

#endif
