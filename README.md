# VIOOVI #

Quick overview of the Vioovi's project. 

### What is Vioovi? ###

* Vioovi Is Our Own Virtual Interface (Recursive acronym).
* See "The Elements of Computing Systems" by Noam Nisan and Shimon Schocken.

![TECS2.jpg](https://bitbucket.org/repo/djG7n4/images/2230173083-TECS2.jpg)

### How to use it? ###

* /* in progress */

### Available Samples ###

* /* in progress */

### Languages and Dependences ###

* Compiler: not finished
* Emulator: C++, SFML, libpthread
* Assembler: OCaml
* VM: C++
* Compiler: C, Bison, Flex

### AUTHORS ###

* Fabien "Saruta" Siron
