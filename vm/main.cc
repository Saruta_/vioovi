#include <iostream>
#include <string>
#include "lexer.hh"
#include "vm.hh"

using namespace std;

int main(int argc, char* argv[]){
  VM vm;
  for (int i = 1; i < argc; i++) {
    Lexer lex(argv[i]);

    for (int i =0; i< lex.getSize(); i++){
      list<string> tokens = lex.tokenize();
      vm.new_line(tokens);
    }

  }
  vm.print();

/*  for (it = tokens2.begin(); it != tokens2.end(); it++)
    cout << *it << endl;

*/

  return 0;
}
