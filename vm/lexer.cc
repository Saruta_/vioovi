#include "lexer.hh"

using namespace std;

Lexer::Lexer(std::string path){
  ifstream file;
  file.open(path);
  if (file.is_open()){

    std::string line;
    while (getline (file, line))
      code.push_back(line);

    file.close();
  } else{

    error = FILE_NOT_FOUND;
    print_error();
  }
  size = code.size();

}

std::list<std::string> Lexer::tokenize(){
  string line= code.front();
  code.pop_front();
  list<std::string> local_tokens;
 // local_tokens.assign(tokens.begin(), tokens.end());

  bool inline_comment = false;
  string token = "";
  list<string> output_line;

  for (int i = 0; i < line.size(); i++){
    char c = line[i];
    if (c != ' '){
      token += c;
    }

    if(token == "//")
      inline_comment = true;

    if (!inline_comment) {
     if (c == ' ' || i == (line.size() -1)){
       if (token != ""){
         output_line.push_back(token);
         token = "";
       }
      }
    }
  }
  return output_line;
}


void Lexer::print_error(){
  if (error == 0)
    std::cout << "no error" << std::endl;
  else if (error == FILE_NOT_FOUND)
    std::cout << "file not found" << std::endl;
  else if (error == TOKEN_NOT_FOUND)
    std::cout << "token not found" << std::endl;
  else {
    std::cout << "other error" << std::endl;
  }

}

int Lexer::getSize(){
  return size;
}



