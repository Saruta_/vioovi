#ifndef _LEXER_H
#define _LEXER_H

#include <iostream>
#include <string>
#include <list>
#include <fstream>

#define FILE_NOT_FOUND 1
#define TOKEN_NOT_FOUND 2

class Lexer{
  private:
    std::list<std::string> code;
    std::list<std::string> tokens;
    int error;
    int size;

  public:
    Lexer(std::string path);
    std::list<std::string> tokenize();
    int getSize();
    void print_error();



};


#endif /* LEXER_H */
