#include "vm.hh"

using namespace std;

VM::VM(){
  count_bool_eq = 0;
  count_bool_gt = 0;
  count_bool_lt = 0;
  count_rec = 0;
  file = "main";

  /* boot */
  as += "@256\nD=A\n@SP\nM=D\n";
  _call ("Sys.init", "0");
 /* as += "@300\nD=A\n@LCL\nM=D\n";
  as += "@400\nD=A\n@ARG\nM=D\n";
  as += "@3000\nD=A\n@THIS\nM=D\n";
  as += "@3010\nD=A\n@THAT\nM=D\n";*/
}

void VM::new_line(list<string> line){
  if (line.size() != 0){
    if(line.front() == "push" || line.front() == "pop"){
     if (line.size() != 3){
       cerr << "error push" << std::endl;
     }
     else{
       string p = line.front();
       line.pop_front();
       string seg = line.front();
       line.pop_front();
       string i = line.front();
       line.pop_front();
       memory_access(p,seg,i);
     }
    } else if(line.front() == "label" || line.front() == "goto" ||
        line.front() == "if-goto"){
      if (line.size() == 2){
        if (line.front() == "label")
        {
          line.pop_front();
          string arg = line.front();
          line.pop_front();
          label(arg);
        }
        else if (line.front() == "goto")
        {
          line.pop_front();
          string arg = line.front();
          line.pop_front();
          _goto(arg);
        }
        else if (line.front() == "if-goto")
        {
          line.pop_front();
          string arg = line.front();
          line.pop_front();
          _if(arg);
        }
      }
    } else if (line.front() == "function" || line.front() == "call" ||
        line.front() == "return"){
      if (line.size() == 3 && line.front() == "function") {
        line.pop_front();
        string label = line.front();
        line.pop_front();
        string nb_loc = line.front();
        line.pop_front();
        _function (label, nb_loc);
      }
      else if (line.size() == 3 && line.front() == "call"){
        line.pop_front();
        string label = line.front();
        line.pop_front();
        string arg = line.front();
        line.pop_front();
        _call (label, arg);
      }
      else if (line.size() == 1 && line.front() == "return"){
        line.pop_front();
        _return();
      }
      else{
        cerr << "error arg fun/call/return" << endl;
      }
      
    }else{
      if (line.size() == 1){
        string command = line.front();
        arithmetic_logic(command);
      }else {
        cerr << "error arg" << endl;
      }
    }
  }
}


void VM::memory_access(string p, string seg, string i){
  if (p == "push"){
    if ("constant" == seg){
        as +="@"+i+"\n"+
              "D=A\n"+
              "@SP\n"+
              "A=M\n"+
              "M=D\n";
    }
    else if ("static" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
        as += "@"+file+"."+add_str+"\n"+
              "D=M\n"+
              "@SP\n"+
              "A=M\n"+
              "M=D\n";
    }
    else if ("that" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n"+
            "D=A\n"+
            "@THAT\n"+
            "A=M+D\n"+
            "D=M\n"+
            "@SP\n"+
            "A=M\n"+
            "M=D\n";
    }
    else if ("this" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n"+
            "D=A\n"+
            "@THIS\n"+
            "A=M+D\n"+
            "D=M\n"+
            "@SP\n"+
            "A=M\n"+
            "M=D\n";
    }
    else if ("argument" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n"+
            "D=A\n"+
            "@ARG\n"+
            "A=M+D\n"+
            "D=M\n"+
            "@SP\n"+
            "A=M\n"+
            "M=D\n";
    }
    else if ("local" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n"+
            "D=A\n"+
            "@LCL\n"+
            "A=M+D\n"+
            "D=M\n"+
            "@SP\n"+
            "A=M\n"+
            "M=D\n";
    }
    else if ("pointer" == seg){
        int add = atoi(i.c_str())+3;
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
        as += "@"+add_str+"\n"+
              "D=M\n"+
              "@SP\n"+
              "A=M\n"+
              "M=D\n";
    }
    else if ("temp" == seg){
        int add = atoi(i.c_str())+5;
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
        as += "@"+add_str+"\n"+
              "D=M\n"+
              "@SP\n"+
              "A=M\n"+
              "M=D\n";
    }
    as += "@SP\nM=M+1\n";
  } else if (p == "pop"){
    as += "@SP\nM=M-1\n";
    if ("static" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
        as += "@SP\n";
        as += "A=M\n";
        as += "D=M\n";
        as += "@"+file+"."+add_str+"\n";
        as += "M=D\n";
    }
    else if("this" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n";
      as += "D=A\n";
      as += "@THIS\n";
      as += "D=D+M\n";
      as += "@SP\n";
      as += "A=M+1\n";
      as += "M=D\n";
      as += "@SP\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "A=A+1\n";
      as += "A=M\n";
      as += "M=D\n";
    }
    else if("that" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n";
      as += "D=A\n";
      as += "@THAT\n";
      as += "D=D+M\n";
      as += "@SP\n";
      as += "A=M+1\n";
      as += "M=D\n";
      as += "@SP\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "A=A+1\n";
      as += "A=M\n";
      as += "M=D\n";

    }
    else if("argument" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n";
      as += "D=A\n";
      as += "@ARG\n";
      as += "D=D+M\n";
      as += "@SP\n";
      as += "A=M+1\n";
      as += "M=D\n";
      as += "@SP\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "A=A+1\n";
      as += "A=M\n";
      as += "M=D\n";

    }
    else if("local" == seg){
        int add = atoi(i.c_str());
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "@"+add_str+"\n";
      as += "D=A\n";
      as += "@LCL\n";
      as += "D=D+M\n";
      as += "@SP\n";
      as += "A=M+1\n";
      as += "M=D\n";
      as += "@SP\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "A=A+1\n";
      as += "A=M\n";
      as += "M=D\n";
    }
    else if("pointer" == seg){
        int add = atoi(i.c_str())+3;
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "A=M\n";
      as += "D=M\n";
      as += "@"+add_str+"\n";
      as += "M=D\n";
    }
    else if("temp" == seg){
        int add = atoi(i.c_str())+5;
      string add_str =  static_cast<ostringstream*>
        (&(ostringstream() << add))->str();
      as += "A=M\n";
      as += "D=M\n";
      as += "@"+add_str+"\n";
      as += "M=D\n";
    } 
    else if("nop" == seg){}
  } else {
    cout << "error pop or push command" << endl;
  }
}

void VM::arithmetic_logic(string command){
  if (command == "add"){
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M+D\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
  }
  else if (command == "sub"){
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M-D\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
  }
  else if (command == "neg"){
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=-M\n";
  }
  else if (command == "and"){
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M&D\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
  }
  else if (command == "or"){
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M|D\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
  }
  else if (command == "not"){
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=!M\n";
  }
  else if (command == "eq"){
      string count =
        static_cast<ostringstream*>(&(ostringstream() << count_bool_eq))->str();
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M-D\n";
      /* if */
      as += "@BOOL.EQ.ELSE."+count+"\n";
      as += "D;JEQ\n";
      /* then */
      as += "@0\n";
      as += "D=A\n";
      as += "@BOOL.EQ.END."+count+"\n";
      as += "0;JMP\n";
      /* else */
      as += "(BOOL.EQ.ELSE."+count+")\n";
      as += "@0\n";
      as += "D=!A\n";
      as += "(BOOL.EQ.END."+count+")\n";
      /* endif */
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
      count_bool_eq++;
  }
  else if (command == "gt"){
      string count =
        static_cast<ostringstream*>(&(ostringstream() << count_bool_gt))->str();
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M-D\n";
      /* if */
      as += "@BOOL.GT.ELSE."+count+"\n";
      as += "D;JGT\n";
      /* then */
      as += "@0\n";
      as += "D=A\n";
      as += "@BOOL.GT.END."+count+"\n";
      as += "0;JMP\n";
      /* else */
      as += "(BOOL.GT.ELSE."+count+")\n";
      as += "@0\n";
      as += "D=!A\n";
      as += "(BOOL.GT.END."+count+")\n";
      /* endif */
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
      count_bool_gt++;
  }
  else if (command == "lt"){
      string count =
        static_cast<ostringstream*>(&(ostringstream() << count_bool_lt))->str();
      as += "@SP\n";
      as += "M=M-1\n";
      as += "A=M\n";
      as += "D=M\n";
      as += "@SP\n";
      as += "A=M-1\n";
      as += "D=M-D\n";
      /* if */
      as += "@BOOL.LT.ELSE."+count+"\n";
      as += "D;JLT\n";
      /* then */
      as += "@0\n";
      as += "D=A\n";
      as += "@BOOL.LT.END."+count+"\n";
      as += "0;JMP\n";
      /* else */
      as += "(BOOL.LT.ELSE."+count+")\n";
      as += "@0\n";
      as += "D=!A\n";
      as += "(BOOL.LT.END."+count+")\n";
      /* endif */
      as += "@SP\n";
      as += "A=M-1\n";
      as += "M=D\n";
      count_bool_lt++;
  }

}

void VM::label(string label){
  as += "("+label+")\n";
}

void VM::_goto(string label){
  as += "@"+label+"\n";
  as += "0;JMP\n";
}

void VM::_if(string label){
  as += "@SP\n";
  as += "M=M-1\n";
  as += "A=M\n";
  as += "D=M\n";
  as += "@"+label+"\n";
  as += "D;JNE\n";
}

void VM::_function(string label_p, string var_p){
  int var = 0;
  istringstream(var_p) >> var;
  label (label_p);
  for (int i = 0; i < var; i++)
    memory_access ("push", "constant", "0");
}

void VM::_call(string label_p, string arg_p){
  /* push addr return*/
  string count = to_string(count_rec);
  ++count_rec;
  memory_access ("push", "constant", ("RETURN_"+count));
  /* push LCL */
  as += "@LCL\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "M=D\n";
  as += "@SP\n";
  as += "M=M+1\n";
  /* push ARG */
  as += "@ARG\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "M=D\n";
  as += "@SP\n";
  as += "M=M+1\n";
  /* push THIS */
  as += "@THIS\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "M=D\n";
  as += "@SP\n";
  as += "M=M+1\n";
  /* push THAT */
  as += "@THAT\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "M=D\n";
  as += "@SP\n";
  as += "M=M+1\n";
  /* ARG = SP-n-5 */
  as += "@"+arg_p+"\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "D=M-D\n"; 
  as += "@5\n";
  as += "D=D-A\n";
  as += "@ARG\n";
  as += "M=D\n";
  /* LCL = SP */
  as += "@SP\n";
  as += "D=M\n";
  as += "@LCL\n";
  as += "M=D\n";
  /* goto f */
  _goto(label_p);
  /* label return */
  label(("RETURN_"+count));
}

void VM::_return(){
  /* FRAME=LCL with FRAME tmp */
  as += "@LCL\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "M=D\n";
  as += "@SP\n";
  as += "M=M+1\n";
  memory_access ("pop", "temp", "0");
  /* RET = *(FRAME-5) */
  memory_access ("push", "temp", "0");
  as += "@5\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "A=M-D\n";
  as += "D=M\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "M=D\n";
  memory_access ("pop", "temp", "1");
  /* *ARG = pop() */
  as += "@SP\n";
  as += "M=M-1\n";
  as += "A=M\n";
  as += "D=M\n";
  as += "@ARG\n";
  as += "A=M\n";
  as += "M=D\n";
  /* SP = ARG+1 */
  as += "@ARG\n";
  as += "D=M\n";
  as += "D=D+1\n";
  as += "@SP\n";
  as += "M=D\n";
  /* THAT = *(FRAME-1) */
  memory_access ("push", "temp", "0");
  as += "@1\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "A=M-D\n";
  as += "D=M\n";
  as += "@THAT\n";
  as += "M=D\n";
  memory_access ("pop", "nop", "0");
  /* THIS = *(FRAME-2) */
  memory_access ("push", "temp", "0");
  as += "@2\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "A=M-D\n";
  as += "D=M\n";
  as += "@THIS\n";
  as += "M=D\n";
  memory_access ("pop", "nop", "0");
  /* ARG = *(FRAME-3) */
  memory_access ("push", "temp", "0");
  as += "@3\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "A=M-D\n";
  as += "D=M\n";
  as += "@ARG\n";
  as += "M=D\n";
  memory_access ("pop", "nop", "0");
  /* LCL = *(FRAME-4) */
  memory_access ("push", "temp", "0");
  as += "@4\n";
  as += "D=A\n";
  as += "@SP\n";
  as += "A=M\n";
  as += "A=A-1\n";
  as += "A=M-D\n";
  as += "D=M\n";
  as += "@LCL\n";
  as += "M=D\n";
  memory_access ("pop", "nop", "0");
  /* goto RET */
  memory_access ("push", "temp", "1"); 
  as += "@SP\n";
  as += "M=M-1\n";
  as += "A=M\n";
  as += "A=M\n";
  as += "0;JMP\n";
}

void VM::print(){
  cout << as << endl;
}
