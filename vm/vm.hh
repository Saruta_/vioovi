#ifndef _VM_H
#define _VM_H

#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <cstdlib>

/*
 *  this section defines all the special
 * values in RAM
 */

#define SP 0
#define LCL 1
#define ARG 2
#define THIS 3
#define THAT 4

#define TRUE "0xffff"
#define FALSE "0x0000"

class VM{
  private:
    int constants[16];
    std::string as;
    int count_bool_eq;
    int count_bool_gt;
    int count_bool_lt;
    int count_rec;
    std::string file;

  public:
    VM();
    void new_line(std::list<std::string> line);
    void memory_access(std::string p, std::string segment,
      std::string index);
    void arithmetic_logic(std::string command);
    void label(std::string label);
    void _goto(std::string label);
    void _if(std::string label);
    void _function(std::string label, std::string var);
    void _call(std::string label, std::string arg);
    void _return();
    void print();
};


#endif /* _VM_H */
