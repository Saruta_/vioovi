open Printf
open Parser
open SymbolTab

let generate_labels path = 
  let file = Parser.init path in
  let rec aux file =
    let line = Parser.clean_line(Parser.advance file) in
      match (Parser.hasMoreCommands line) with
        | false ->()
        | true ->
            begin
              match (Parser.commandType line) with
              | "LABEL" -> Parser.load_label line; (aux file)
              | "C_COMMAND"  | "A_COMMAND" -> SymbolTab.incPC (); aux file
              | _ -> aux file
            end
  in aux file;;


let pre_generate_binary path =
  let file = Parser.init path in
  let rec aux file =  
    let line = Parser.clean_line(Parser.advance file) in
      match (Parser.hasMoreCommands line) with
        | false -> ()
        | true ->  
          begin
            match (Parser.commandType line) with
            | "EOF" | "COMMENT" | "EMPTY" -> aux file
            | "ERROR_PARSING"-> invalid_arg "error parsing"
            | "C_COMMAND" -> SymbolTab.incPC (); aux file
	        | "LABEL" -> (aux file)
            | "A_COMMAND" ->  SymbolTab.incPC ();Parser.load_var line; (aux file)
            | _ -> failwith "error from parser" 
          end 
  in aux file;;

let generate_binary path =
  let file = Parser.init path in
  let rec aux file =  
    let line = Parser.clean_line(Parser.advance file) in
      match (Parser.hasMoreCommands line) with
        | false -> ""
        | true ->  
          begin
            match (Parser.commandType line) with
            | "EOF" | "COMMENT" | "EMPTY" | "LABEL" -> "" ^ (aux file)
            | "ERROR_PARSING"-> invalid_arg "error parsing"
            | "C_COMMAND" -> "111" ^ (Parser.comp line) ^
                                    (Parser.dest line) ^
                                   (Parser.jump line) ^ "\n" ^
                                   (aux file)
            | "A_COMMAND" -> Parser.a_command(line) ^ "\n"^ (aux file) 
            | _ -> failwith "error from parser" 
          end 
  in aux file;;

let enum_lines path =
  let file = Parser.init path in
  let rec aux file =
    let line = Parser.advance file in
    match (Parser.hasMoreCommands line) with
      | false -> print_endline "eof"
      | true -> print_int (String.length line); print_endline " "; aux file
  in aux file;;

let generate_file out binary =
  let file = open_out out in
    fprintf file "%s" binary;
    close_out file;;


let () =
  if not((Array.length Sys.argv) = 3) then
    invalid_arg "Command format: >Assembler file_path out_name"
  else
    begin
      if not(Sys.file_exists(Sys.argv.(1))) then
        invalid_arg "File doesn't exists."
      else
        begin
          let path = Sys.argv.(1) and out = Sys.argv.(2) in
            (*enum_lines path*)
             generate_labels path;
             pre_generate_binary path;
             let binary = generate_binary path in
               generate_file out binary;
               print_endline "generation success";
        end
    end;;

