module Base_converter = struct
let rec list_of_string str =
  let rec aux str = function
    | n when n=(String.length str) -> []
    | n when str.[n]= 'a' -> 10:: aux str (n+1)
    | n when str.[n]= 'b' -> 11:: aux str (n+1)
    | n when str.[n]= 'c' -> 12:: aux str (n+1)
    | n when str.[n]= 'd' -> 13:: aux str (n+1)
    | n when str.[n]= 'e' -> 14:: aux str (n+1)
    | n when str.[n]= 'f' -> 15:: aux str (n+1)
    | n -> (int_of_string(Char.escaped(str.[n]))) :: aux str (n+1)
  in aux str 0 

let rec string_of_list = function
  | [] -> ""
  | h::t -> (string_of_int h) ^ (string_of_list t)

let rec completion = function
  | str when (String.length str) < 16 -> completion ("0"^str)
  | str -> str

(* Append 2 lists  ==> @*)

let rec append l1 l2 = 
	match l1 with
		|[] -> l2
		|h::t -> h:: append t l2


(* reversing a list *)
let rec reverse l =
	let rec aux ac l = match l with
		|[] -> ac
		|h::t -> aux (h::ac) t
	in
	aux [] l


(* set an int to the power of n *)

let rec power x = function
	|0 -> 1
	|n -> x * power x (n-1)
 

(* convert_from base 10 *)

let rec ten_to_base n b =
	match (n mod b) with
		|0 when n/b = 0 -> [] (*end of conversion *)
		|r -> append(ten_to_base (n/b) b) [r]


(* convert into base 10 *)

let base_to_ten l b =
	let rec aux n = function 
		|[] -> 0
		|h::t -> h * (power b n) + (aux (n+1) t)  
	in 
	aux 0 (reverse l) 

end;;
