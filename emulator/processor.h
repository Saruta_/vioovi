#ifndef _PROCESSOR_H
#define _PROCESSOR_H

#include <iostream>
#include <stdint.h>
#include <cstring>
#include <string>
#include <fstream>

#define NOT_ASSIGNED 0x8000

#define RAM_SIZE 24577
#define ROM_SIZE 32768

/* masks for instruction bit-masking */
#define MASK_AC 0x8000
#define MASK_COMP 0x1fc0
#define MASK_DEST 0x0038
#define MASK_JUMP 0x0007

class Processor{

    private:
        int16_t ram[RAM_SIZE];
        uint16_t rom[ROM_SIZE];

        int pc_max;

        /* registers */
        int16_t a;
        int16_t d;
        uint16_t pc;

    public:
        Processor();
        bool loadROM(char * path);
        int16_t ALU();
        bool compute();
        void debug();
        bool is_instruction();
        int16_t* get_ram();



};

#endif

