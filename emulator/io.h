#ifndef _IO_H
#define _IO_H

#include <iostream>
#include <stdint.h>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#define WINDOW_BASE 16384

class IO{
  private:
    int16_t *ram;
    sf::RenderWindow* window;

  public:
    IO(int16_t *ram_p);
    void display();
    void boot_routine();
    void get_events();
    ~IO();



};

#endif
