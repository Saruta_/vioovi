#include "io.h"


IO::IO(int16_t *ram_p){
  std::string vioovi = "vioovi";
  ram = ram_p;
  window = new sf::RenderWindow(sf::VideoMode(512,256), vioovi);
}

void IO::get_events(){
  int16_t event = 0;
  /*for (int i =0; i<26; i++){
    sf::Keyboard::Key key = static_cast<sf::Keyboard::Key>(i);
    if (sf::Keyboard::isKeyPressed(key))
      event = i+0x61;
  }*/
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    event = 130;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    event = 132;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    event = 131;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    event = 133;

  ram[24576] = event;
}

void IO::boot_routine(){
  sf::Font font;
  if (font.loadFromFile("lucida-console.ttf")){
    sf::Text text;
    text.setFont(font);
    text.setString("> VIOOVI is booting...");
    text.setCharacterSize(12);
    text.setColor(sf::Color::White);

    for (int i=0; i<120;i++){
      window->clear();
      window->draw(text);
      window->display();
    }
    window->clear(sf::Color::Black);
    window->display();
  }
}

void IO::display(){
  while (1){
    window->clear(sf::Color::Black);
    sf::RectangleShape rect (sf::Vector2f(1,1));
    rect.setOutlineColor(sf::Color::White);
    rect.setFillColor(sf::Color::White);

    int r = 0;
    int16_t nmb;

    for (int i = WINDOW_BASE; i<24576;i++){
      if (ram[i]){
        nmb = ram[i];
        for (int j =15; j>=0; j--){
          r = nmb %2;
          nmb = nmb >>1;
          if (r){
            rect.setPosition(((i-WINDOW_BASE)%32)*16+(15-j),(i-WINDOW_BASE)/32);
            window->draw(rect);
          }

        }
      }
    }
    window->display();
    get_events();

  }
}

IO::~IO(){
  delete window;
}

