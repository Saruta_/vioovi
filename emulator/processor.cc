#include "processor.h"

using namespace std;



Processor::Processor(){
    pc = 0;
    for (int i =0; i< RAM_SIZE; i++)
      ram[i] = 0;

    //for (int i =0; i< ROM_SIZE; i++)
    //  rom[i] = NOT_ASSIGNED;

}

bool Processor::loadROM(char* path){
    ifstream file (path, ios::in);

    if (!file){
        cerr << "fail file" << endl;
        return false;
    } else{
        string line;
        int j =0;
        while (getline(file, line)){
            int ratio = 1;
            uint16_t acc = 0;
            for(int i =15; i>=0; i--){
               if (line[i] == '1')
                   acc += ratio;
               ratio *= 2;
            }
            rom[j] = acc;
            j++;
        }
        pc_max = j;

        file.close();
        return true;
    }
}


bool Processor::compute(){
  if (!(rom[pc] & MASK_AC)){
    // @ instruction
    a = rom[pc];
    pc++;

  }else{
    // C instruction
    int16_t result = ALU();

    switch ((rom[pc] & MASK_DEST)>>3){
      case 0x1:
        ram[a] = result;
      break;
      case 0x2:
        d = result;
      break;
      case 0x3:
        ram[a] = result;
        d = result;
      break;
      case 0x4:
        a = result;
      break;
      case 0x5:
        ram[a] = result;
        a = result;
      break;
      case 0x6:
        a = result;
        d = result;
      break;
      case 0x7:
        ram[a] = result;
        a = result;
        d = result;
      break;
    }

    switch((rom[pc] & MASK_JUMP)){
      case 0x1:
        if (result > 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x2:
        if (result == 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x3:
        if (result >= 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x4:
        if (result < 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x5:
        if (result != 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x6:
        if (result <= 0)
          pc = a;
        else
          ++pc;
      break;
      case 0x7:
        pc = a;
      break;
      default:
        pc++;
    }


  }


}

int16_t Processor::ALU(){
  switch ((rom[pc] & MASK_COMP)>>0x06){
    case 0x2a:
      return 0;
    break;
    case 0x3f:
      return 1;
    break;
    case 0x3a:
      return -1;
    break;
    case 0x0c:
      return d;
    break;
    case 0x30:
      return a;
    break;
    case 0x0d:
      return ~d;
    break;
    case 0x31:
      return ~a;
    break;
    case 0x0f:
      return -d;
    break;
    case 0x33:
      return -a;
    break;
    case 0x1f:
      return d+1;
    break;
    case 0x37:
      return a+1;
    break;
    case 0x0e:
      return d-1;
    break;
    case 0x32:
      return a-1;
    break;
    case 0x02:
      return d+a;
    break;
    case 0x13:
      return d-a;
    break;
    case 0x07:
      return a-d;
    break;
    case 0x00:
      return d&a;
    break;
    case 0x15:
      return d|a;
    break;
    case 0x70:
      return ram[a];
    break;
    case 0x71:
      return ~ram[a];
    break;
    case 0x73:
      return -ram[a];
    break;
    case 0x77:
      return ram[a]+1;
    break;
    case 0x72:
      return ram[a]-1;
    break;
    case 0x42:
      return d+ram[a];
    break;
    case 0x53:
      return d-ram[a];
    break;
    case 0x47:
      return ram[a]-d;
    break;
    case 0x40:
      return d&ram[a];
    break;
    case 0x55:
      return d|ram[a];
    break;
  }

}

void Processor::debug(){
  std::cout << "******Registers******" << std::endl;
  std::cout << "PC: " << pc << std::endl;
  std::cout << "A: " << a << std::endl;
  std::cout << "D: " << d << std::endl;
  std::cout << "********RAM**********" <<std::endl;
  for (int i =0; i< 16; i++)
    std::cout << i <<": " << ram[i] << std::endl;
 // std::cout << "********ROM**********" <<std::endl;
 // for (int i =0; i< 16; i++)
 //   std::cout << i <<": " << rom[i] << std::endl;
  std::cout << "******end debug******" << std::endl <<std::endl;
}

bool Processor::is_instruction(){
  return pc < pc_max;
}

int16_t *Processor::get_ram(){
  return ram;
}
