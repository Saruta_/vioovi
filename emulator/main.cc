#include <iostream>
#include <thread>
#include "processor.h"
#include "io.h"
#include <unistd.h>



using namespace std;

void display_screen(IO* io_p){
  io_p->display();
}


int main(int argc, char* argv[]){

  Processor processor;
  IO io(processor.get_ram());
  io.boot_routine();
  thread my_screen(display_screen, &io);

  if (!processor.loadROM(argv[1]))
    return 1;

  do{
    processor.compute();
   // processor.debug();
   for (int i =0; i<100;i++){}

  }while(processor.is_instruction());

  return 0;
}
